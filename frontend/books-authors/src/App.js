import Home from "./Routes/Home";
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import BookPage from "./Routes/BookPage"

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <div className="App">
      <Router>
        <Route path="/" exact render={(props) => <Home />} />
        <Route path="/book-page/:id" exact render={(props) => <BookPage />} />
      </Router>
    </div>
      </header>
    </div>
  );
}

export default App;
