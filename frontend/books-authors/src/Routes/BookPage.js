import React from "react";
import Axios from "axios";
import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";


function BookPage() {
    let { id } = useParams();
    const [book, setBook] = useState(null);
    useEffect(() => {
        console.log('id............',id);
        // Axios.get(`http//localhost:8080/book/${id}`).then(
        //   (response) => {
        //     console.log(response.data);
        //     setBook(response.data.data);
        //   }
        // );
        Axios.get(`http://localhost:8080/book/${id}`).then((response) => {
          console.log(response.data);
        })
      }, []);

      if(book) {
        return (<div className="book-Info">
        <h1>{book.bookName}</h1>
        <div className="bookPage-Data">
            <div className="bookPage-Row">
              <h3 className="bookPage-RowHeader">ISBN</h3>
              <h3 className="bookPage-RowData">{book.isbn}</h3>
            </div>
        </div>
        <div className="authtor-Data">
            <div className="authtor-Row">
              <h3 className="authtor-RowHeader">FirstName</h3>
              <h3 className="authtor-RowData">{book.authtor.firstName}</h3>
            </div>
            <div className="authtor-Row">
              <h3 className="authtor-RowHeader">LastName</h3>
              <h3 className="authtor-RowData">{book.authtor.lastName}</h3>
            </div>
        </div>
        </div>
        
        )
      }else{
          return (<h1>something went wrong...</h1>);
      }
      
}
export default BookPage;