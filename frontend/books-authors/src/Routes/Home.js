import Axios from "axios";
import { useState, useEffect } from "react";
import Book from "../Components/Book.js";
function Home() {
  const [books, setBooks] = useState([]);
  useEffect(() => {
    Axios.get("http://localhost:8080/books").then((res) => {
      console.log(res.data);
      setBooks(res.data.data);
    });
  }, []);
  return <div className="bookContainer">
      {books.map((book) =>{
          return <Book bookName={book.bookName} id={book.id} key={book.id}/>
      })}
  </div>;
}
export default Home;
