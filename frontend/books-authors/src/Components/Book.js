import React from "react";
import { useHistory } from "react-router-dom";

const Book = ({ bookName, id }) => {
  let history = useHistory();
  return (
    <div className="bookContainer">
      <div className="bookrow">
        <h1 className="bookName">{bookName}</h1>
        <button
          onClick={() => {
            history.push(`/book-page/${id}`);
          }}
        >
          More Info
        </button>
      </div>
    </div>
  );
};
export default Book;
