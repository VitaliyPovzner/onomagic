const db = require("../config/db.config.js");
const Book = db.book;
const Authtor =db.author;
// Post a Book
exports.create = (req, res) => {
  Book.create({
    bookName: req.body.bookName,
    isbn: req.body.isbn,
    author: req.body.author,
  }).then((book) => {
    res.status(200).json({
      status: true,
      message: "Book created successfully",
    });
  });
};

// Get all books
exports.findAll = (req, res) => {
  Book.findAll().then((books) => {
    // Send all books as response
    res.status(200).json({
      status: true,
      data: books,
    });
  });
};

// Find a book by Id
exports.findByPk = (req, res) => {
  Book.findByPk(req.params.id,{
    include: [{
      model: Authtor, as: "author"
    }]
  }).then((book) => {
    console.log(req.params)
    res.status(200).json({
      status: true,
      data: book,
    });
  }).catch((err) => {console.log(err)});
};

// Update a book
exports.update = (req, res) => {
  const id = req.params.bookId;
  Book.update(
    {
      bookName: req.body.bookName,
      isbn: req.body.isbn,
      author: req.body.author
    },
    { where: { id: req.params.bookId } }
  ).then(() => {
    res.status(200).json({
        status: true,
        message: "Book updated successfully with id = " + id
    });
  });
};

