const db = require("../config/db.config.js");
const Author = db.author;

// Post an Author
exports.create = (req, res) => {
  Author.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  }).then((author) => {
    res.status(200).json({
      data : author
    });
  });
};

// Get all authors
exports.findAll = (req, res) => {
  Author.findAll().then((authors) => {
    res.status(200).json({
      data: authors
    });
  });
};

// Find a author by Id
exports.findByPk = (req, res) => {
  Author.findByPk(req.params.authorId).then((author) => {
    res.status(200).json({
      data: author
    });
  });
};

// Update a author
exports.update = (req, res) => {
  const id = req.params.authorId;
  Author.update(
    {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
    },
    { where: { id: req.params.authorId } }
  ).then((author) => {
    res.status(200).json({
      data : author
    });
  });
};
