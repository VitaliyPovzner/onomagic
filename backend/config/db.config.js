const Sequelize = require("sequelize");
const env = require("./env.js");
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
  port: env.port,
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Models
db.book = require("../models/Book")(sequelize, Sequelize);
db.author = require("../models/Authtor")(sequelize, Sequelize);

module.exports = db;
