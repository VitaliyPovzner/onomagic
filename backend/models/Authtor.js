module.exports = (sequelize, Sequelize) => {
  const Author = sequelize.define(
    "author",
    {
      id:{
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING,
      },
      lastName: {
        type: Sequelize.STRING,
      },
    },
    {
      timestamps: false,
    }
  );

  return Author;
};
