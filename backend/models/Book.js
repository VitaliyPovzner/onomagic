module.exports = (sequelize, Sequelize) => {
  const Authtor = require("./Authtor")(sequelize, Sequelize);
  const Book = sequelize.define(
    "book",
    {
      bookName: {
        type: Sequelize.STRING(),
      },
      isbn: {
        type: Sequelize.STRING,
      },
      authorId: {
        type: Sequelize.INTEGER,
        references: {
          model: Authtor,
          key: "id",
        },
      },
    },
    {
      timestamps: false,
    }
  );
  Authtor.hasMany(Book, { as: "books" });
  Book.belongsTo(Authtor, {
    foreignKey: "authorId",
    as: "author",
  });
  return Book;
};
