const express = require("express"),
  router = express.Router();
const book = require("../service/bookService");

//books
router.get("/books", book.findAll);
router.get("/book/:id", book.findByPk);
router.post("/",book.create);
router.put("/book",book.update);


//authors
router.get("/author/:id", book.findByPk);
router.post("/author",book.create);
router.put("/author",book.update);

module.exports = router;
