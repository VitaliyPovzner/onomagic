/// express application
const express = require("express");
const app = express();
const port = process.env.PORT || 8080;
const bodyParser = require("body-parser");
const cors = require("cors");
app.use(cors());
app.use(bodyParser.json());
const db = require("./config/db.config.js");

db.sequelize.sync().then(
  () => {
    console.log("");
  },
  (err) => console.log(err)
);
const controller = require("./controllers/controller");
app.use(`/`, controller);

app.use((req, res) => {
  res.status(404).json({
    message: "Path not found",
  });
});

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
